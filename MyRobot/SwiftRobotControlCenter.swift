//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    var prevHasCandy: Bool = false
	
	
	//in this function change levelName
	override func viewDidLoad() {
		levelName = "L555H" // level name
		
		super.viewDidLoad()
		
	}
	
	//write your code here
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
        
        while true {
            buildHorizontalLine()
            putIfNeed()
            checkAndTurnDown()
            if frontIsBlocked && leftIsBlocked{
                break
            }
            safeMove()
            checkAndTurnLeft()
            buildHorizontalLine()
            putIfNeed()
            checkAndTurnDown()
            if frontIsBlocked && rightIsBlocked{
                break
            }
            safeMove()
            checkAndTurnRight()
        }
	}
    
    func buildHorizontalLine(){
        while true {
            putIfNeed()
            safeMove()
            if !frontIsClear{
                break
            }
        }
    }
    
    func putIfNeed() {
        print("put if Need prev \(prevHasCandy)")
        if !candyPresent && !prevHasCandy{
            put()
            prevHasCandy = true
        }else {
            prevHasCandy = false
        }
    }
    
    func backStepUp(){
        while !facingUp {
            turnRight()
        }
        safeMove()
    }
    
    func backStepLeft(){
        while !facingLeft {
            turnRight()
        }
        safeMove()
    }
    
    func doubleMove(){
        safeMove()
        safeMove()
    }
    
    func checkAndTurnDown() {
        while !facingDown {
            turnRight()
        }
    }

    func checkAndTurnLeft() {
        while !facingLeft {
            turnRight()
        }
    }

    func checkAndTurnRight() {
        while !facingRight {
            turnRight()
        }
    }

    func checkAndTurnUp() {
        while !facingUp {
            turnRight()
        }
    }
    
    func safeMove () {
        if frontIsClear {
            move()
        }
    }
}
